//
//  AlertOnboardingLibrary.h
//  AlertOnboardingLibrary
//
//  Created by Henry Heguang Miao on 23/8/17.
//  Copyright © 2017 CookMinute. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for AlertOnboardingLibrary.
FOUNDATION_EXPORT double AlertOnboardingLibraryVersionNumber;

//! Project version string for AlertOnboardingLibrary.
FOUNDATION_EXPORT const unsigned char AlertOnboardingLibraryVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AlertOnboardingLibrary/PublicHeader.h>


